# ❄️  My NixOS flake templates

This is a collection of [nix flake](https://nixos.wiki/wiki/Flakes) templates I made.
You can find documentation in the respective subfolders.
To use one of these flakes, run:

```bash
# If you have a poetry project with pre-built, binary Python dependencies from
# PyPI and want to run it on NixOS
nix flake init -t gitlab:nobodyinperson/flakes#poetry2nix

# If you want to run a poetry project inside an FHS environment on NixOS
# (e.g. because the above poetry2nix doesn't work)
nix flake init -t gitlab:nobodyinperson/flakes#poetryFHS
```
