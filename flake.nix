{
  description = "Yann's nix flake templates";
  outputs = { self, nixpkgs }: {
    templates = {
      poetryFHS = {
        path = ./poetryFHS;
        description = "A Python poetry project in an FHS environment";
      };
      poetry2nix = {
        path = ./poetry2nix;
        description = "A Python project with poetry2nix";
      };
    };
  };
}
