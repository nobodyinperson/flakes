# Nix Flake Template for `poetry2nix` - Python development under NixOS

Python development under NixOS is not much different from other distros - **until** you work with pre-compiled packages (`numpy`, `scipy`, etc...) involving system libraries (`libstdc++`, `libz`, you know the ImportErrors...).
Here, you have two quick'n'dirty (and arguably bad) options:

- Use the required packages from nixpkgs, e.g. with `nix-shell -p python310Packages.numpy` (it's the version from nixpkgs then, you need to pin nixpkgs  or override the package yourself to get a different version)
- Manually telling Python where to look for these libraries via `LD_LIBRARY_PATH` (discouraged, error-prone, fragile and cumbersome)

Instead, [`poetry2nix`](https://github.com/nix-community/poetry2nix) allows to work properly on a [poetry](https://python-poetry.org/)-based Python project on NixOS.
Poetry is a very solid tool and using it for your Python projects makes sense anyway (venv management, dependency locking, sane config file, etc.).
The `flake.nix` in this example repository should be all you need (at least it has been for me) to add to your project to get the following working:

```bash
# Use 'nix develop' (instead of 'poetry shell' and 'poetry install') to enter a shell in which you do your Python development with all packages available.
# You need to exit and rerun if you change dependencies
nix develop

# If your Python project shipts scripts, you can run them like this outside 'nix develop'
nix run . -- bla bli blubb # from within the repo
nix run git+https://server.com/your/repo -- bla bli blubb  # from anywhere

# Enter a shell with your installed project and scripts available
nix shell # from within the repo
nix shell git+https://server.com/your/repo # from anywhere
```

## Usage

To use this flake, either just copy the `flake.nix` into your Python project or run the following (e.g. next to your `poetry.lock`, `pyproject.toml`, etc.):

```bash
nix flake init -t gitlab:nobodyinperson/flakes#poetry2nix
```
