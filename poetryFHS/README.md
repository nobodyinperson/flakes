# Nix Flake Template for `poetry` in an FHS environment - Python development under NixOS

Python development under NixOS is not much different from other distros - **until** you work with pre-compiled packages (`numpy`, `scipy`, etc...) involving system libraries (`libstdc++`, `libz`, you know the ImportErrors...).
Here, you have two quick'n'dirty (and arguably bad) options:

- Use the required packages from nixpkgs, e.g. with `nix-shell -p python310Packages.numpy` (it's the version from nixpkgs then, you need to pin nixpkgs  or override the package yourself to get a different version)
- Manually telling Python where to look for these libraries via `LD_LIBRARY_PATH` (discouraged, error-prone, fragile and cumbersome)

If not even [`poetry2nix`](https://github.com/nix-community/poetry2nix) works for you (e.g. with the [`poetry2nix` flake in this repo](`https://gitlab.com/nobodyinperson/flakes/-/tree/main/poetry2nix?ref_type=heads`)), then another option is to use [`buildFHSUserEnv`](https://ryantm.github.io/nixpkgs/builders/special/fhs-environments/) to make an FHS-compliant environment to run your poetry in.
Poetry is a very solid tool and using it for your Python projects makes sense anyway (venv management, dependency locking, sane config file, etc.).
The `flake.nix` in this example repository should be all you need (at least it has been for me) to add to your project to get the following working:

```bash
# Use 'nix develop' (effectively runs 'poetry shell' and 'poetry install' for you) to enter a shell in which you do your Python development with all packages available.
# You need to exit and rerun if you change dependencies
nix develop
```

As far as I know, with `buildFHSUserEnv` it is not directly possible to `nix run` the project 🫤

## Usage

To use this flake, either just copy the `flake.nix` into your Python project or run the following (e.g. next to your `poetry.lock`, `pyproject.toml`, etc.):

```bash
nix flake init -t gitlab:nobodyinperson/flakes#poetryFHS
```

## Misc

If you see weird `tkinter` importing errors, you can try adding this to the `profile` script in `buildFHSUserEnv`:

```bash
if ! python -c 'import tkinter' 2>/dev/null >/dev/null;then
  # Fix tkinter import (shouldn't be necessary, but it is... Including tkinter in targetPkgs doesn't help)
  # The hard-coded 3.11 version might need updating here, don't know how to automate this elegantly 🤷
  export PYTHONPATH="${
    toString pkgs.python3Packages.tkinter
  }/lib/python3.11/site-packages/:$PYTHONPATH"
fi
```
