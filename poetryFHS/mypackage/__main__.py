# python-only dependency, works on NixOS also without flake
import sys
from rich import print


def cli():
    try:
        # this fails on NixOS with a 'pip install' or 'poetry install'ed numpy
        import numpy as np

        print(f"Here you have a numpy array: {np.array([1,2,3,4])}")
        print(f"You gave arguments: {sys.argv[1:]}")
        print(f"✅ If you can see this, numpy works! 🥳")
    except ImportError:
        print("🙄 Couldn't import numpy... Here's the traceback:")
        raise


if __name__ == "__main__":
    cli()
